<header class="navbar navbar-expand-lg navbar-light bg-brown fixed-top" aria-label="Main navigation"> <!-- css inline -->
  <div class="container-fluid">
    <a class="navbar-brand text-white" href="#">
    	<img src="./assets/img/logo.png" width="30px" class="rounded bg-white">
    </a>
    <button class="navbar-toggler p-0 border-0" type="button" data-bs-toggle="offcanvas" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>

    <div class="navbar-collapse offcanvas-collapse" id="navbarsExampleDefault">
      <ul class="navbar-nav me-auto mb-2 mb-lg-0">
	  	<li class="nav-item">
		  <a class="nav-link text-white" href="index.php" >Home</a>
	  	</li>
	  	<li class="nav-item">
		  <a class="nav-link text-white" href="index.php?menu=image" >Image</a>
	  	</li>
	  	<li class="nav-item">
      <a class="nav-link text-white" href="image.php" >Another page</a>
      </li>
      <li class="nav-item">
		  <a class="nav-link text-white" href="index.php?menu=testimonial" >Form Testimonial</a>
	  	</li>


	</ul>
    </div>
  </div>
</header>