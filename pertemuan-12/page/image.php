<div id="image-component" class="bg-white py-5">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<h2 class="display-5 fw-normal">Penggunaan Image</h2>	
				<p class="lead fw-normal">
					Tag <code>&lt;img&gt;</code> digunakan untuk menyematkan gambar di halaman HTML.
					Gambar tidak secara teknis dimasukkan ke dalam halaman web; gambar ditautkan ke halaman web. Tag <code>&lt;img&gt;</code> membuat ruang penyimpanan untuk gambar yang direferensikan.

					Tag <code>&lt;img&gt;</code> memiliki dua atribut yang diperlukan:
				</p>
				<p class="text-center bg-light py-2">
					<code class="language-html" data-lang="html">
						<span class="p">&lt;</span><span class="nt">img</span> <span class="na">src</span><span class="o">=</span><span class="s">"..."</span> <span class="na">alt</span><span class="o">=</span><span class="s">"..."</span> </span> <span class="na">width</span><span class="o">=</span><span class="s">"...px"</span> </span> <span class="na">height</span><span class="o">=</span><span class="s">"..."</span> <span class="p">&gt;</span>
					</code>
				</p>	
			</div>
		</div> 

	</div>
</div>

<div id="latihan-1-component" class="py-5">
	<div class="container">					
		<div class="row">
			<div class="col-12">
				<h2 class="display-5 fw-normal">Latihan Image dan Table</h2>	
				<div class="card border-0">
					<div class="card-body">
						<div class="row">
							<div class="col-6">
								<h5>Family Information</h5>
							</div>
							<div class="col-6 text-right">
								<button class="btn btn-primary btn-sm font-weight-bolder ">Add New</button>
							</div>
						</div>
						<div class="table-responsive mt-3">
							<table class="table table-head-custom table-head-bg table-borderless table-vertical-center">
								<thead>
									<tr class="text-left text-uppercase bg-light">
										<th colspan="2" style="min-width: 250px" class="pl-7">
											<span class="text-dark-75">Name</span>
										</th>
										<th style="min-width: 100px">Gender</th>
										<th style="min-width: 100px">Birthdate</th>
										<th style="min-width: 100px">Education</th>
										<th style="min-width: 130px">Occupation</th>
										<th style="min-width: 80px"></th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td class="pl-0 py-8" width="60">
											<img src="./assets/img/001-boy.svg" class="align-self-end bg-light p-1 rounded" alt="" width="100%">
										</td>
										<td class="pl-0 py-8">
											<a href="#" class="text-dark-75 font-weight-bolder text-hover-primary mb-1 font-size-lg">Septian Cahyadi </a>
											<span class="text-muted font-weight-bold d-block">Father</span>
										</td>
										<td>
											<span class="text-muted font-weight-bold">Male</span>
										</td>
										<td>
											<span class="text-dark-75 font-weight-bolder d-block font-size-lg">Bekasi</span>
											<span class="text-muted font-weight-bold">15 September 1980</span>
										</td>
										<td>
											<span class="text-dark-75 font-weight-bolder d-block font-size-lg">S2</span>
											<span class="text-muted font-weight-bold">Univ Indonesia</span>
										</td>
										<td>
											<span class="text-muted font-weight-bold d-block font-size-sm">Pengajar</span>
										</td>
										<td class="pr-0 text-right">
											<div class="btn-group">			
											<a href="#" class="btn btn-success font-weight-bolder btn-sm">Edit</a>
											<a href="#" class="btn btn-danger font-weight-bolder btn-sm">Delete</a>
											</div>
										</td>
									</tr>

									<tr>
										<td class="pl-0 py-8" width="60">
											<img src="./assets/img/002-girl.svg" class="align-self-end bg-light p-1 rounded" alt="" width="100%">
										</td>
										<td class="pl-0 py-8">
											<a href="#" class="text-dark-75 font-weight-bolder text-hover-primary mb-1 font-size-lg">Rahayu Ningsih </a>
											<span class="text-muted font-weight-bold d-block">Mother</span>
										</td>
										<td>
											<span class="text-muted font-weight-bold">Female</span>
										</td>
										<td>
											<span class="text-dark-75 font-weight-bolder d-block font-size-lg">Depok</span>
											<span class="text-muted font-weight-bold">25 Juli 1985</span>
										</td>
										<td>
											<span class="text-dark-75 font-weight-bolder d-block font-size-lg">SMA</span>
											<span class="text-muted font-weight-bold">SMA Depok</span>
										</td>
										<td>
											<span class="text-muted font-weight-bold d-block font-size-sm">Housewife</span>
										</td>
										<td class="pr-0 text-right">
											<div class="btn-group">			
											<a href="#" class="btn btn-success font-weight-bolder btn-sm">Edit</a>
											<a href="#" class="btn btn-danger font-weight-bolder btn-sm">Delete</a>
											</div>
										</td>
									</tr>
									<tr>
										<td colspan="7" class="text-center">==Dan seterusnya, minimal 5 buah data. Dimana masing-masing mahasiswa memiliki nilai data yang berbeda-beda==</td>
									</tr>

								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<div id="component" class="bg-white py-5">
	<div class="container">
		<div class="row d-none">
			<div class="col-12">
				<ul>
					<li>satu</li>
					<li>dua</li>
				</ul>
				<ol>
					<li>test</li>
					<li>test</li>
				</ol>
				<p>
					<span>Family info</span>
					<button class="btn btn-info right"><span>Add</span> <span>New</span></button>
				</p>
				<div class="row">
					<div class="col-6 bg-info"><span>Family info</span></div>
					<div class="col-6 bg-danger text-right"><button class="btn btn-info">Add new</button></div>
				</div>
			</div>
		</div>


		<h2 class="display-5 fw-normal">Review Materi</h2>
		<div class="review-mtr">
			<div class="baris-kolum">
				<p>Pembuatan baris & kolom</p>
				<div class="row">
					<div class="col-12 bg-info">col-12 = w:100%</div>
					<div class="col-1 bg-primary">col-1</div>
					<div class="col-1 bg-danger">col-1</div>
					<div class="col-1 bg-primary">col-1</div>
					<div class="col-1 bg-danger">col-1</div>
					<div class="col-1 bg-primary">col-1</div>
					<div class="col-1 bg-danger">col-1</div>
					<div class="col-1 bg-primary">col-1</div>
					<div class="col-1 bg-danger">col-1</div>
					<div class="col-1 bg-primary">col-1</div>
					<div class="col-1 bg-danger">col-1</div>
					<div class="col-1 bg-primary">col-1</div>
					<div class="col-1 bg-danger">col-1</div>
				</div>
				<div class="row">
					<div class="col-6 bg-info">col-6</div>
					<div class="col-4 bg-success">col-4</div>
					<div class="col-2 bg-warning">col-3</div>
				</div>
			</div>

			<div class="rev-table">
				
			</div>

		</div>
	</div>
</div>

<div id="container-component">
	<h2 class="display-5 fw-normal">Container</h2>
	<div class="container bg-warning">
		container
	</div>
	<div class="container-fluid bg-danger">
		container-fluid
	</div>
</div>

<div id="rev-image-component" class="py-5">
	<div class="container">
		<div class="row">
			<div class="col-3">
				<div class="rv-img">
					<h2 class="display-5 fw-normal">Image</h2>
					<style type="text/css">
					.bg-img-avatar{
						background-color: purple;
						padding: 10px;
						border-radius: 100%; /*membuat sisi melengkung*/
						width: 150px;
						height: 150px;
					}
					</style>
					<img src="./assets/img/005-girl-2.svg" alt="Gambar Logo IBIK" class="bg-img-avatar">
				</div>
				<div class="rv-img-bg">
					<style type="text/css">
						.sample-bg{
							background-image: url('./assets/img/background.jpg')
						}
					</style>
					<h2 class="display-5 fw-normal">Background Image</h2>
					<div class="sample-bg">Background Image</div>
					<p class="sample-bg">Bg in paragraf</p>
					<button class="sample-bg">BG in button</button>
				</div>
			</div>
			<div class="col-3">
				<div class="rv-table">
					<h2 class="display-5 fw-normal">Table</h2>
					<table border="1">
						<thead>
							<tr>
							    <th>Month</th>
							    <th colspan="2">Savings</th>
							    <th>Savings</th>
							    <th>Savings</th>
						  	</tr>		
						</thead>
						<tbody>
							<tr>
							    <td>January</td>
							    <td colspan="3" class="bg-info">$100</td>									    
							    <td>$100</td>
						  	</tr>
						  	<tr>
							    <td rowspan="3" class="bg-danger">January</td>
							    <td>$100</td>
							    <td>$100</td>
							    <td>$100</td>
							    <td>$100</td>
						  	</tr>
						  	<tr>
							    <td>$100</td>
							    <td>$100</td>
							    <td>$100</td>
							    <td>$100</td>
						  	</tr>
						  	<tr>
							    <td>$100</td>
							    <td>$100</td>
							    <td>$100</td>
							    <td>$100</td>
						  	</tr>

						</tbody>
					</table>
				</div>
			</div>
			<div class="col-6">
				<div class="rv-link">
					<h2 class="display-5 fw-normal">Link</h2>
					<a href="https://detik.com">Link 1</a>
					<a >Link 2</a>
					<a href="">Link 3</a>
					<a href="#image-component">Link 4</a>
					<a href="https://detik.com" target="_blank">Link 5 akan membuka tab baru</a>
					<a href="https://detik.com" target="_self">Link 6</a>
				</div>
				<div class="rv-button">
					<h2 class="display-5 fw-normal">Button</h2>
					<p>Manual HTML</p>
					<style type="text/css">
						.btn-manual-green{
							border:1px solid green;
							background-color: green;
							color: white
						}
					</style>

					<input type="button" value="Button 2" />
					<input type="reset" value="Button 2" class="btn-manual-green" />
					<input type="submit" value="Button 2" /> <br />
					<button>Button 1</button>								
					<button type="reset">Button Reset</button>
					<button type="submit" class="btn-manual-green">Button Submit</button>
					<button type="button">Button Button</button>

					<p>CSS Bootstrap</p>
					<button type="button" class="btn btn-success" >Button Green</button>
					<button type="button" class="btn btn-danger" >Button Merah</button>
					<button type="button" class="btn btn-info" >Button Biru</button>
					<button type="button" class="btn btn-warning" >Button Kuning</button>

				</div>
			</div>
		</div>
							
	</div>
</div>


<div id="form-element" class="py-5 bg-white">
	<div class="container">
		<h2 class="display-5 fw-normal">Forms</h2>
		<div class="cmpt-form">
			<form action="#" method="post" autocomplete="off" >
				<div class="row">
					<div class="col-3">
						<div class="card">
							<div class="card-body">
								<h4>Element input</h4>
								<div class="form-group">
									<label>Text box</label>
									<input type="text" required name="id" value="" class="form-control" placeholder="isi username" />
								</div>	
								<div class="form-group">
									<label>Password box</label>
									<input type="password" name="password" value="" class="form-control" placeholder="isi password" />
								</div>	
								
								<div class="form-group">
									<label>Email box</label>
									<input type="email" readonly name="email" value="test@gmail.com" class="form-control" />
								</div>	
								<div class="form-group">
									<label>Date box</label>
									<input type="date" disabled name="date" value="" class="form-control" placeholder="isi password" />
								</div>	
								<div class="form-group">
									<label>Number box</label>
									<input type="number" name="number" value="" class="form-control" placeholder="isi password" />
								</div>	
								<div class="form-group">
									<label>Color box</label>
									<input type="color" name="color" value="" class="form-control" placeholder="isi password" />
								</div>

								<div class="form-group">
									<label>Radio</label><br>
									<label>
										<input type="radio" name="gender" checked value="M"/> Male											
									</label>
									<input type="radio" name="gender" value="F"/> Female
								</div>											
								<div class="form-group">
									<label>Checkbox</label><br>
									<label>
										<input type="checkbox" name="type" value="1"/> Satu
									</label>
									<input type="checkbox" name="type" value="2"/> Dua
									<input type="checkbox" name="type" checked value="3"/> Tiga
								</div>
							</div>
						</div>
					</div>

					<div class="col-3">
						<div class="card">
							<div class="card-body">
								<h4>Select box</h4>
								<div class="form-group">
									<label>Select</label>
									<select name="agama" class="form-control">
										<option value="">Pilih satu</option>
										<option value="1">Islam</option>
										<option value="2" selected >Kristen</option>
										<option value="3" disabled >Katolik</option>
										<option value="4">Budha</option>
										<option value="5">Hindu</option>
									</select>
								</div>
							</div>
						</div>
						<div class="card mt-3">
							<div class="card-body">
								<h4>Attribute</h4>
								<p>required = form isian harus di isi</p>
								<p>readonly = form isian tidak bisa ditulis</p>
								<p>disabled = form isian tidak bisa ditulis & dikirim</p>
							</div>
						</div>

					</div>

					<div class="col-3">
						<div class="card">
							<div class="card-body">
								<h4>Text area</h4>
								<div class="form-group">
									<label>Textarea</label>	
									<textarea class="form-control" rows="10" cols="2" name="desc" placeholder="Isi lah isian ini " ></textarea>											
								</div>
							</div>
						</div>
					</div>


					<div class="col-3">
						<div class="card">
							<div class="card-body">
								<h4>Button</h4>
								<div class="form-group">
									<button class="btn btn-info" type="button">Button</button>
									<input class="btn btn-info" type="button" value="Button" />
								</div>	
								<div class="form-group">
									<button class="btn btn-warning" type="reset">Reset</button>
									<input class="btn btn-warning" type="reset" value="Reset" />
								</div>	
								<div class="form-group">
									<button class="btn btn-primary" type="submit">Submit</button>
									<input class="btn btn-primary" type="submit" value="Submit" />
								</div>
							</div>
						</div>									
					</div>

					<div class="col-3 mt-3">
						<div class="card">
							<div class="card-header">
								<h1>Judul</h1>
							</div>
							<div class="card-body">
								<p>isi elemen</p>
							</div>
						</div>
					</div>

				</div>	
			</form>
		</div>
	</div>
</div>