<?php 
date_default_timezone_set("Asia/Bangkok"); //+7
$servername = "localhost";
$username = "root";
$password = "";

$db = "ibik_pw_2021_db";
// Create connection
$conn = mysqli_connect($servername, $username, $password,$db);

$baseurl = "http://localhost/program/IBIK/pemrograman-web-rk/pertemuan-12/";

// Check connection
if (!$conn) {
    echo "Error: Unable to connect to MySQL." . PHP_EOL;
    echo "Debugging errno: " . mysqli_connect_errno() . PHP_EOL;
    echo "Debugging error: " . mysqli_connect_error() . PHP_EOL;
    exit;
}

?>