<div class="container">

<?php if(!empty($_GET['message'])){ ?>
	<div class="alert alert-warning alert-dismissible fade show" role="alert">
	  <strong>Pesan</strong> <?=$_GET['message']?>
	</div>
<?php } ?>

<form id="form-testi" action="page/post_testimonial.php" method="post" class="mt-5 mb-5">
	<div class="form-group">
		<label>Email</label>
		<input type="email" name="email" class="form-control" >
	</div>
	<div class="form-group">
		<label>Nama Lengkap</label>
		<input type="text" name="name" class="form-control" >
	</div>
	<div class="form-group">
		<label>Pesan</label>
		<textarea class="form-control" name="message"></textarea>
	</div>
	<div class="form-group">
		<label>Jenis Kelamin</label>
		<p><label><input type="radio" name="gender" value="M" /> Pria</label>
		<label><input type="radio" name="gender" value="F" /> Wanita</label></p>
	</div>

	<button class="btn btn-info" type="reset">Reset</button>
	<button class="btn btn-info" type="submit">Submit</button>


</form>

<?php 
include "conections.php";
$query = "SELECT * FROM `testimonial`";
$showData = mysqli_query($conn,$query);

echo "<h1>Formating Date</h1>";
//echo date("d-m-Y H:i:s"); //menampilkan tanggal sekarang
$tanggal = "2021-06-01 10:00:56";
$convertString2Time = strtotime($tanggal);
$formatTanggal = date("d-m-Y H:i",$convertString2Time);
echo "Tanggal:".$tanggal;
echo "<br>Hasil convert ke time :".$convertString2Time;
echo "<br>Format tgl:".$formatTanggal;

?>
	<div id="show-testimonial">
		<h1>Daftar testimonial</h1>
		<table class="table table-bordered">
			<thead>
				<tr>
					<th>No</th>
					<th>Email</th>
					<th>Nama</th>
					<th>Pesan</th>
					<th>Gender</th>
					<th>Posted At</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
			<?php 
			//check apakah data pada database ada atau tidak
			if($showData->num_rows > 0){
				$no=1;
				//looping kedalam database
				while ($row = mysqli_fetch_array($showData)) {
					$gender = "-";
					if($row['gender'] == 'M'){
						$gender = "Pria";
					}else{
						$gender = "Wanita";
					}


				 ?>					
				<tr>
					<td><?=$no?></td>
					<td><?=$row['email']?></td>
					<td><?=$row['name']?></td>
					<td><?=$row['message']?></td>
					<!-- <td><?=$gender?></td> -->
					<td><?= ($row['gender'] == 'M') ? 'Pria':'Wanita' ?></td>
					<td><?= date('d-M-Y H:m', strtotime($row['createdAt'])) //converting string ke dalam format date ?></td>
					<td>
						<div class="btn-group">
							<button class="btn btn-warning" type="button">Edit</button>
							<button class="btn btn-danger" type="button">Hapus</button>
						</div>
					</td>
				</tr>
			<?php $no++; } ?>
			<?php }else{ ?>
				<tr>
					<td colspan="6">Data kosong</td>
				</tr>
			<?php } ?>
				
			</tbody>
		</table>
	</div>
</div>