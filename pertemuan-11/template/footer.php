<footer class="bd-footer p-2 p-md-3 bg-light text-left text-sm-start">
  <div class="container">
    <p class="mb-0">Design by Febri Damatraseta Fairuz, S.T, M.Kom.</p>
    <p class="mb-0">Copyright &copy; 2020 by IBI Kesatuan Bogor Jurusan Teknologi Informasi.</p>
  </div>
</footer>