<!DOCTYPE html>
<html>
	<head>
		<title>Web Programming IBIK</title>

		<!-- Required meta tags -->
	    <meta charset="utf-8">
	    <meta name="viewport" content="width=device-width, initial-scale=1">

	    <meta name="author" content="Web Programming IBIK">
	    <meta name="description" content="Landing page">
		
		<?php include "./template/header-script.php"; ?>		

	</head>
	<body class="bg-light">

		<?php include "./template/header.php"; ?>	
		<?php 
			//short if 
			//$menu = ( ($menu == 'home') ? include "./page/home.php" : 'Error 404' );

			//tipe macam2 passing parameter di php
			$menu = ( (!empty($_GET['menu'])) ? $_GET['menu'] : '' ) ;
			//$menu = $_POST['menu']; //=> untuk form submit
			//$menu = $_REQUEST['menu'];
			/*echo '<h2>'.$menu.'</h2>';
			echo '<h2>$menu =>salah</h2>';
			echo "<h2>{$menu}</h2>";*/
			if($menu == 'image'){
				include "./page/image.php";
			}else{
				include "./page/home.php";
			}
			
		?>
		
		
		<?php include "./template/footer.php"; ?>	

		<?php include "./template/footer-script.php"; ?>	
	</body>
</html>












