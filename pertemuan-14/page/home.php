<main>
	<div id="first-component">
		<div class="container">
			<div class="row">
				<div class="col-sm-6 col-md-6">
					<div class="fst-layer py-3">
						<h1>Build fast, responsive sites with Bootstrap</h1>
						<p class="text-left">Quickly design and customize responsive mobile-first sites with Bootstrap, the world’s most popular front-end open source toolkit, featuring Sass variables and mixins, responsive grid system, extensive prebuilt components, and powerful JavaScript plugins.</p>
						
						<a class="btn btn-primary btn-lg">Get Started</a>
						<button class="btn btn-outline-success btn-lg">Download</button>
					</div>							
				</div>
				<div class="col-sm-6 col-md-6 text-center pt-5">
					<img src="./assets/img/pic-1.png" 
						width="50%" 
						height="auto"
						alt="Ini gambar 1"
						title="PIC 1"
						/>
				</div>
			</div>
			<br/><br/>
		</div>
	</div>
	
	
	<div id="table-component" class="py-5">
		<div class="container">					
			<div class="row">
				<div class="col-6">							
					<h2 class="display-5 fw-normal">Penggunaan Table</h2>	
					<p class="lead fw-normal">
				        Dokumentasi dan contoh untuk gaya keikutsertaan tabel (mengingat penggunaannya yang umum di plugin JavaScript) dengan Bootstrap</a>.
			      	</p>	
			      	<p>Pembuatan table pada HTML pada dasarnya diawali dengan taglib <code>&lt;table&gt; ... &lt;/table&gt;</code> dimana didalamnya dibagi menjadi 3 buah bagian yaitu <code>&lt;thead/&gt;</code> <code>&lt;tbody/&gt;</code> dan <code>&lt;tfoot/&gt;</code>. Untuk membuat sebuah baris baru menggunakan <code>&lt;tr/&gt;</code>, sedangkan untuk membuat kolom pada baris menyisipkan <code>&lt;td&gt;</code> ataupun <code>&lt;th/&gt;</code> pada elemen <code>&lt;tr/&gt;</code>.
			      	</p>
			      	<a class="btn btn-lg btn-info mb-3" href="https://www.w3schools.com/tags/tag_table.asp" target="_blank" title="Kunjungi halaman">Dok HTML Table</a>
			      	<a class="btn btn-lg btn-outline-primary mb-3" href="https://getbootstrap.com/docs/5.0/content/tables/" target="_blank" title="Kunjungi halaman">Dok Bootstrap Table</a>
				</div>
				<div class="col-6">
					<div class="card mb-5">
						<div class="card-body">
							<h5 class="card-title">Contoh Table HTML & CSS manual</h5>
							<div class="table-responsive">
								<table border="1" width="100%" >
									<thead>
										<tr class="bg-info">
											<th width="5%">No</th>
											<th width="10%">NPM</th>
											<th>Nama</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>1</td>
											<td class="bg-danger">123456</td>
											<td>Udin</td>
										</tr>
										
										<tr>
											<td>2</td>
											<td>5665565</td>
											<td>Asep</td>
										</tr>
										
										<tr>
											<td>3</td>
											<td>8888</td>
											<td>Agus</td>
										</tr>
										
									</tbody>
									<tfooter>
										<tr>
											<td colspan="3" rowspan="1">Note: bla bla</td>
										</tr>
									</tfooter>
								</table>
							</div>	
						</div>
					</div>
					<div class="card">
						<div class="card-body">
							<h5 class="card-title">Contoh Table pada Bootstrap</h5>
							<div class="table-responsive">
								<table class="table table-striped table-bordered">
									<thead>
										<tr>
											<th width="5%">No</th>
											<th width="10%">NPM</th>
											<th>Nama</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>1</td>
											<td>123456</td>
											<td>Udin</td>
										</tr>
										
										<tr>
											<td>2</td>
											<td>5665565</td>
											<td>Asep</td>
										</tr>
										
										<tr>
											<td>3</td>
											<td>8888</td>
											<td>Agus</td>
										</tr>
										
									</tbody>
									<tfooter>
										<tr>
											<td colspan="3" rowspan="1">Note: bla bla</td>
										</tr>
									</tfooter>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
			
		</div>
	</div>


	


	
	
</main>