<div class="container">

<?php if(!empty($_GET['message'])){ ?>
	<div class="alert alert-warning alert-dismissible fade show" role="alert">
	  <strong>Pesan</strong> <?=$_GET['message']?>
	</div>
<?php } ?>

<?php 
include "conections.php";
$query = "SELECT * FROM `testimonial`";
$showData = mysqli_query($conn,$query);

echo "<h1>Formating Date</h1>";
//echo date("d-m-Y H:i:s"); //menampilkan tanggal sekarang
$tanggal = "2021-06-01 10:00:56";
$convertString2Time = strtotime($tanggal);
$formatTanggal = date("d-m-Y H:i",$convertString2Time);
echo "Tanggal:".$tanggal;
echo "<br>Hasil convert ke time :".$convertString2Time;
echo "<br>Format tgl:".$formatTanggal;

?>

<?php 
$retrive = array();
if(!empty($_GET['ID'])){
	$decID = base64_decode($_GET['ID']);
	$query1 = "select * from testimonial where ID = ".$decID;
	$showData1 = mysqli_query($conn, $query1);
	$row1 = mysqli_fetch_array($showData1);
	$retrive = $row1;

	if(!empty($_GET['action'])){
		$queryDelete = "DELETE FROM `testimonial` WHERE ID = ".$decID;
		$deleteData = mysqli_query($conn,$queryDelete);
		if($deleteData){
			$message = "Berhasil dihapus";
		}else{
			$message = "Gagal dihapus";
		}

		//pindah halaman setelah post data
		header("Location:".$baseurl."index.php?menu=testimonial&message=".$message);
	}
}

//encrypting 
/*hash(algo, data);
base64_encode(data);
md5(str);*/
?>

<form id="form-testi" action="page/post_testimonial.php?action=postData" method="post" class="mt-5 mb-5">
	<div class="form-group">
		<label>Email</label>
		<input type="hidden" name="ID" class="ID"  value="<?=(!empty($retrive) ? $retrive['ID'] : '') ?>">
		<input type="email" name="email" class="form-control email" value="<?=(!empty($retrive) ? $retrive['email'] : '') ?>" >
	</div>
	<div class="form-group">
		<label>Nama Lengkap</label>
		<input type="text" name="name" class="form-control name" value="<?=(!empty($retrive) ? $retrive['name'] : '') ?>" >
	</div>
	<div class="form-group">
		<label>Pesan</label>
		<textarea class="form-control message" name="message"> <?=(!empty($retrive) ? $retrive['message'] : '') ?></textarea>
	</div>
	<div class="form-group">
		<label>Jenis Kelamin</label>
		<p><label><input type="radio" name="gender" class="gender" value="M" <?=(!empty($retrive) ? (($retrive['gender'] == 'M') ? 'checked':'') : '')?> /> Pria</label>
		<label><input type="radio" name="gender" class="gender" value="F" <?=(!empty($retrive) ? (($retrive['gender'] == 'F') ? 'checked':'') : '')?> /> Wanita</label></p>
	</div>

	<button class="btn btn-info" type="reset">Reset</button>
	<button class="btn btn-info" type="submit">Submit</button>

</form>


	<div id="show-testimonial">
		<h1>Daftar testimonial</h1>
		<table class="table table-bordered">
			<thead>
				<tr>
					<th>No</th>
					<th>Email</th>
					<th>Nama</th>
					<th>Pesan</th>
					<th>Gender</th>
					<th>Posted At</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
			<?php 
			//check apakah data pada database ada atau tidak
			if($showData->num_rows > 0){
				$no=1;
				//looping kedalam database
				while ($row = mysqli_fetch_array($showData)) {
					$gender = "-";
					if($row['gender'] == 'M'){
						$gender = "Pria";
					}else{
						$gender = "Wanita";
					}


				 ?>					
				<tr>
					<td><?=$no?></td>
					<td><?=$row['email']?></td>
					<td><?=$row['name']?></td>
					<td><?=$row['message']?></td>
					<!-- <td><?=$gender?></td> -->
					<td><?= ($row['gender'] == 'M') ? 'Pria':'Wanita' ?></td>
					<td><?= date('d-M-Y H:m', strtotime($row['createdAt'])) //converting string ke dalam format date ?></td>
					<td>
						<div class="btn-group">
							<button class="btn btn-warning btn-edit" type="button" data-id="<?=$row['ID']?>" >Edit</button>
							<?php $encID = base64_encode($row['ID']);?>
							<a class="btn btn-info" href="?menu=testimonial&ID=<?=$encID?>">Edit</a>
							<a class="btn btn-danger" href="?menu=testimonial&ID=<?=$encID?>&action=hapus">Hapus</a>
						</div>
					</td>
				</tr>
			<?php $no++; } ?>
			<?php }else{ ?>
				<tr>
					<td colspan="6">Data kosong</td>
				</tr>
			<?php } ?>
				
			</tbody>
		</table>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function(){
		//$(".btn-edit").click(function(){}); => semua class yg namanya btn-edit di dalam body itu dieksekusi
		$("#show-testimonial > .table .btn-edit").click(function(){ // hanya mengakses class btn-edit yg berada didalam parent show-testimonial
			var itsme = $(this); //sy adalah btn-edit / class dirinya sendiri
			var id = itsme.data('id');
			//alert("ID:"+id);
			//AJAX
			$.ajax({
		        type: "POST",
		        url: "page/post_testimonial.php?action=retrive",
		        data: "ID="+id,
		        beforeSend: function(){
		        	itsme.text("loading");
		        },success: function (responce) {
		          	console.log(responce);//syntak menulis di tab console
	        	  	itsme.text("Edit");
	        	  	//fecthing hasil data
	        	  	//responce = dalam bentuk string	
	        	  	//JSON.parse => converting dari string ke object        	  	
	        	  	$.each(JSON.parse(responce),function(key,value){ //looping object menggunakan for / while / do while
	        	  		//save ke dalam form
	        	  		if(key == "gender"){
	        	  			//setting radio button
	        	  			$("#form-testi").find(".gender[value="+value+"]").prop("checked",true);	        	  			
	        	  		}else{
		        	  		//mencari classname yg sama dengan key namenya
		        	  		//jika sama maka inset value
		        	  		//$("#form-testi").find(".email").val(febrifayruz@yahoo.co.id);
	        	  			$("#form-testi").find("."+key).val(value);
	        	  		}
	        	  	});
		        },error: function(e) {
		            console.log("Error "+e);
		        }
	      	});
	      	//end ajax

		});
	});
</script>